/*
 * Filename: constants.h
 
 * Description: 
 * Holds all the constants for AST Assignment 2 :: Makefiles

 * Author: Colin Mills

 * Date: 2015/02/05
 * Date Due: 2015/02/13 
*/


// Game Related Constants //
#define RANGE 10


// CheckIfHighOrLow() return status //
#define TOO_HIGH 1
#define TOO_LOW -1
#define SUCCESS 0 

// UI Related Constants //
#define BUFFER_SIZE 8


