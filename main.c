

/*
 * Filename: main.c

 * Description: 
 * Holds all the main logic for AST Assignment 2 :: Makefiles
 * A project to demonstrate the use of makefiles
 * Write a simple game that checks if things are higher or lower but compile it with makefiles.

 * Author: Colin Mills

 * Date: 2015/02/05
 * Date Due: 2015/02/13 

*/


// Includes //

// System Headers //
#include <stdio.h>

// Project Headers //
#include "constants.h"



int main(void)
{
    // Variable Definition //
    int retStatus = 0;

    int randomNumber = 0;
    int playerChoice = 0;
    int highOrLowStatus = 0;
    int running = 1;


    // Logic //

    // Make this static //
    printf("\n Random Number Guesser \n");

    randomNumber = generateRandomNumber();

    while(running)
    {
        // We set the player choice and the random number. 
        playerChoice = getUserInput();

        // Check to see if its high or low. //
        highOrLowStatus = checkIfHighOrLow(playerChoice, randomNumber);

        // Now we print the appropriate message //

        if (highOrLowStatus == TOO_HIGH)
        {
            printf("\n The value %d is too high \n", playerChoice);
        }

        else if (highOrLowStatus == TOO_LOW)
        {
            printf("\n The value %d is too low \n", playerChoice);
        }

        else
        {
            printf("\n We have a winner. \n");
            running = 0;
        }
    }

    return (retStatus);
}

