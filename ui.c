/*
 * Filename: ui.c
 
 * Description: 
 * Holds all the code for the user interface in the AST Assignment 2 :: Makefile

 * Author: Colin Mills

 * Date: 2015/02/05
 * Date Due: 2015/02/13

*/


// Includes // 

// System Headers // 
#include <stdio.h>
#include <string.h> // 

// Project Headers // 
#include "constants.h"



int getUserInput()
{
    
    // Variable Definition //
    char buffer[BUFFER_SIZE] = "";
    int number = 0;

    
    // Logic //
    printf("\n Enter a number: \n");
    printf("\n >>>");
    fgets(buffer, BUFFER_SIZE, stdin);

    if (isdigit(buffer[0]) != 0)
    {
        number = atoi(buffer);
    }

    else
    {
        printf("\nPlease enter a number\n");
    }


    return(number);


}


void clearNewLine(char* buffer)
{
      
}
