/*
 * Filename: game.c 
 
 * Description: 
 * Holds the game logic for AST :: Assignment 2 :: Makefiles

 * Author: Colin Mills

 * Date: 2015/02/05
 * Date Due: 2015/02/13 

*/

// Includes //

// System Headers //
#include <stdio.h>
#include <stdlib.h> // srand()
#include <time.h>

// Project Headers //
#include "constants.h"
#include "prototypes.h"

/*

 * Function Name: generateRandomNumber(int, int)

 * Description: 
 * Generates random number.

 * Parameters:
 * void 

 * Return Values:
 * Returns an int which is the number. 

*/

int generateRandomNumber(void)
{
    // Variable Definition //
    int randomNumber = 0;

    // Logic //

    // Generate a seed // 
    srand(time(NULL)); 

    // Get the random number //
    randomNumber = 1 + rand() % RANGE; 

    return (randomNumber);
}

/*

 * Function Name: checkIfHighOrLow(int, int)

 * Description: 
 * Takes a guess and a random number and checks to see its placement.
 *

 * Parameters:

   * int randomValue: The random value we generated.  
   * int playerGuess: The players guess that we get.  
 
 * Return Values:

 * An int representing a return status that can be:
    -  1:  Too high.
    - -1:  Too low.
    -  0:  Correct.
 * 

*/

int checkIfHighOrLow(int playerGuess, int randomNumber)
{
    // Variable Definition //
    int retStatus = 0;
    
    // Logic //
    
    // Check to see if it is greater then

    if (playerGuess > randomNumber)
    {
        retStatus = TOO_HIGH;  
    }

    else if (playerGuess < randomNumber)
    {
        retStatus = TOO_LOW;
    }

    else
    {
        retStatus = SUCCESS; 
    }

    return (retStatus);
}










